package com.kidandtiger.app;

import com.kidandtiger.R;

import android.app.DialogFragment;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

public class AboutDialog extends DialogFragment {
	
	public AboutDialog() {}

	@Override
	public View onCreateView(LayoutInflater i, ViewGroup c, Bundle s) {
		View view = i.inflate(R.layout.f_about, c);
		getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
		getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
		return view;
	}

}
