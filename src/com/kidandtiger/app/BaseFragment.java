package com.kidandtiger.app;

import android.app.Fragment;
import android.os.Bundle;
import android.widget.TextView;

public class BaseFragment extends Fragment {
    protected boolean mVolumeOn;    // i think this isn't to be used
    protected boolean mNarratorOn;  // same as this one
    protected TextView mNarration;  // and this one
    // TODO Dialog to navigate through pages is the common function for all pages 
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}