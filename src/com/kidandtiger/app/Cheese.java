package com.kidandtiger.app;

import com.kidandtiger.R;

/* static class */
public class Cheese {
    
    // do not re-arrange the values of the indices
    public static final int[] BACKGROUNDS = {
        R.raw.sfx01,
        R.raw.sfx02,
        R.raw.sfx03,
        R.raw.sfx04,
        R.raw.sfx05,
        R.raw.sfx06,
        R.raw.sfx07,
        R.raw.sfx08,
        R.raw.sfx00
    };
    
    // do not re-arrange the values of the indices
    public static final int[] SUBTITLES = {
        R.raw.page1,
        R.raw.page2,
        R.raw.page3,
        R.raw.page4,
        R.raw.page5,
        R.raw.page6,
        R.raw.page7,
        R.raw.page8,
    };
    
    public static final String PAGE01 = FragmentPage01.class.getName();
    public static final String PAGE02 = FragmentPage02.class.getName();
    public static final String PAGE03 = FragmentPage03.class.getName();
    public static final String PAGE04 = FragmentPage04.class.getName();
    public static final String PAGE05 = FragmentPage05.class.getName();
    public static final String PAGE06 = FragmentPage06.class.getName();
    public static final String PAGE07 = FragmentPage07.class.getName();
    public static final String PAGE08 = FragmentPage08.class.getName();
    public static final String PAGECR = FragmentPageCredits.class.getName();
    
}
