package com.kidandtiger.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.kidandtiger.R;

public class CreditsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.a_credits);
	}
	
	public void navigate(View view) {
		switch (view.getId()) {
		case R.id.previous_credits:
			Intent j = new Intent(this, Page08Activity.class);
			j.putExtra("volume", KidAndTigerApp.getInstance().getVolumeFlag());
			j.putExtra("bg_volume", KidAndTigerApp.getInstance().getNarratorFlag());
			startActivity(j);
			break;

		default:
			break;
		}
	}

}
