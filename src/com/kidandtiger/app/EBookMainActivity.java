package com.kidandtiger.app;

import static com.kidandtiger.app.Cheese.PAGE01;
import static com.kidandtiger.app.Cheese.PAGE02;
import static com.kidandtiger.app.Cheese.PAGE03;
import static com.kidandtiger.app.Cheese.PAGE04;
import static com.kidandtiger.app.Cheese.PAGE05;
import static com.kidandtiger.app.Cheese.PAGE06;
import static com.kidandtiger.app.Cheese.PAGE07;
import static com.kidandtiger.app.Cheese.PAGE08;
import static com.kidandtiger.app.Cheese.PAGECR;

import java.util.ArrayList;
import java.util.List;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.kidandtiger.R;
import com.kidandtiger.util.DebugLog;

public class EBookMainActivity extends FragmentActivity implements ViewPager.OnPageChangeListener {
    private boolean mMute = false;
    private ViewPager mPager;
    private static final String TAG = "EBookMainActivity";
    
    private MediaPlayer mSfx;
    private MediaPlayer mSub;
    
    /* current fragment position */
    private int mPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ebook_a_main);
        DebugLog.d(TAG, "onCreate");

        // add fragments
        List<Fragment> fragments = new ArrayList<Fragment>();
        fragments.add(Fragment.instantiate(this, PAGE01));
        fragments.add(Fragment.instantiate(this, PAGE02));
        fragments.add(Fragment.instantiate(this, PAGE03));
        fragments.add(Fragment.instantiate(this, PAGE04));
        fragments.add(Fragment.instantiate(this, PAGE05));
        fragments.add(Fragment.instantiate(this, PAGE06));
        fragments.add(Fragment.instantiate(this, PAGE07));
        fragments.add(Fragment.instantiate(this, PAGE08));
        fragments.add(Fragment.instantiate(this, PAGECR));

        // adapter
        EBookAdapter adapter = new EBookAdapter(getSupportFragmentManager(), fragments);

        // pager
        mPager = (ViewPager) findViewById(R.id.view_pager);
        mPager.setAdapter(adapter);
        mPager.setOnPageChangeListener(this);
        
        playBackground(mPosition);
        playSubtitle(mPosition);
    }

    @Override
    protected void onPause() {
        super.onPause();
        DebugLog.d(TAG, "onPause");
        
        if (mSfx != null) {
            mSfx.stop();
            mSfx.release();
            mSfx = null;
        }
        
        if (mSub != null) {
            mSub.stop();
            mSub.release();
            mSub = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        DebugLog.d(TAG, "onResume");
        
        if (mPosition == 8) {
            playBackground(mPosition);
        } else {
            playBackground(mPosition);
            playSubtitle(mPosition);
        }
        
        if (mMute) {
            mute();
        } else {
            unMute();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        DebugLog.d(TAG, "onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        DebugLog.d(TAG, "onStop");
    }

    // defined on android:onClick
    public void showSettingsClick(View view) {
    }
    
    // defined on android:onClick
    public void muteClick(View view) {
        switch (view.getId()) {
        case R.id.btn_mute_tiger:
            if (!mMute) {
                mute();
            } else {
                unMute();
            }
            break;

        default:
            break;
        }
    }

    public class EBookAdapter extends FragmentStatePagerAdapter {
        List<Fragment> fragments;

        EBookAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        DebugLog.d(TAG, "fragment position =" + position);
        
        mPosition = position;
        
        if (position >= 1 && position <= 7) {
            playBackground(position);
            playSubtitle(position);
            DebugLog.d(TAG, "playing background_sounds=" + position);
            
        } else if (position == 8) {
            playBackground(position);
            if (mSub != null) {
                mSub.stop();
                mSub.release();
                mSub = null;
            }
            DebugLog.d(TAG, "playing background_sounds=" + position);
            
        } else {
            playBackground(mPosition);
            playSubtitle(mPosition);
            DebugLog.d(TAG, "playing background_sounds=" + position);
        }
        
    }
    
    private void playBackground(int position) {
        if (mSfx != null) {
            mSfx.stop();
            mSfx.release();
            
            mSfx = MediaPlayer.create(this, Cheese.BACKGROUNDS[position]);
            mSfx.start();
            mSfx.setLooping(true);
        
        } else {
            mSfx = MediaPlayer.create(this, Cheese.BACKGROUNDS[position]);
            mSfx.start();
            mSfx.setLooping(true);
        } 
        
        if (!mMute) {
            mute();
        } else {
            unMute();
        }
    }
    
    private void playSubtitle(int position) {
        if (mSub != null) {
            mSub.stop();
            mSub.release();
            
            mSub = MediaPlayer.create(this, Cheese.SUBTITLES[position]);
            mSub.start();
            mSub.setLooping(false);
        } else {
            mSub = MediaPlayer.create(this, Cheese.SUBTITLES[position]);
            mSub.start();
            mSub.setLooping(false);
        }
        
        if (!mMute) {
            mute();
        } else {
            unMute();
        }
    }
    
    private void mute() {
        mMute = true;
        
        if (mSfx != null) {
            mSfx.setVolume(0.0f, 0.0f);
        }
        
        if (mSub != null) {
            mSub.setVolume(0.0f, 0.0f);
        }
    }
    
    private void unMute() {
        mMute = false;
        
        if (mSfx != null) {
            mSfx.setVolume(1.0f, 1.0f);
        }
        
        if (mSub != null) {
            mSub.setVolume(1.0f, 1.0f);
        }
    }
}
