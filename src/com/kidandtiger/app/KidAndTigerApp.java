package com.kidandtiger.app;

import android.app.Application;
import android.content.SharedPreferences;

public class KidAndTigerApp extends Application {
	private static SharedPreferences mPref;
	private static KidAndTigerApp mInstance;
	
	private static final String USER_PREF =        "user_pref";
	private static final String BG_VOLUME_PREF =   "background_volume_pref";
	private static final String BG_NARRATOR_PREF = "background_narrator_pref";
	private static final String INTERVAL =         "interval";
	
	public KidAndTigerApp() {
		super();
		mInstance = this;
	}

	@Override
	public void onCreate() {
		super.onCreate();
	}
	
	public static KidAndTigerApp getInstance() {
		if (mInstance == null) {
			synchronized (KidAndTigerApp.class) {
				if (mInstance == null)
					new KidAndTigerApp();
			}
		}
		
		if (mPref == null)
			mPref = mInstance.getSharedPreferences(USER_PREF, MODE_PRIVATE);
		
		return mInstance;
	}
	
	public void setVolumeFlag(boolean isVolumeOn) {
		SharedPreferences.Editor editor = mPref.edit();
		editor.putBoolean(BG_VOLUME_PREF, isVolumeOn);
		editor.commit();
	}
	
	public boolean getVolumeFlag() {
		return mPref.getBoolean(BG_VOLUME_PREF, true);
	}
	
	public void setNarratorFlag(boolean isNarratorOn) {
		SharedPreferences.Editor editor = mPref.edit();
		editor.putBoolean(BG_NARRATOR_PREF, isNarratorOn);
		editor.commit();
	}
	
	public boolean getNarratorFlag() {
		return mPref.getBoolean(BG_NARRATOR_PREF, true);
	}
	
	public void setInterval(long millis) {
		SharedPreferences.Editor editor = mPref.edit();
		editor.putLong(INTERVAL, millis);
		editor.commit();
	}
	
	public long getInterval() {
		return mPref.getLong(INTERVAL, 0);
	}
	
}
