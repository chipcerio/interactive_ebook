package com.kidandtiger.app;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.kidandtiger.R;

public class MainActivity extends Activity {
	public static int PAGES;
	protected MediaPlayer mSfx;
	private SettingsDialog2 mSettings2;
	private AboutDialog mAbout;
	protected boolean isVolumeOn = true;
	protected boolean isNarratorOn = true;
	private Button sound;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.a_main);
		
		isVolumeOn = getIntent().getBooleanExtra("volume", true);
		isNarratorOn = getIntent().getBooleanExtra("bg_volume", true);
		KidAndTigerApp.getInstance().setVolumeFlag(isVolumeOn);
		KidAndTigerApp.getInstance().setNarratorFlag(isNarratorOn);
		sound = (Button)findViewById(R.id.volume_main);
		
		mSettings2 = new SettingsDialog2(this);
		mAbout = new AboutDialog();

		mSfx = MediaPlayer.create(this, R.raw.sfx00);
		mSfx.setLooping(true);
		mSfx.start();

		if (isVolumeOn){
			sound.setBackgroundResource(R.drawable.sounds_style);
			mSfx.setVolume(1.0f, 1.0f);
		}
		else{
			sound.setBackgroundResource(R.drawable.sound_mute);
	
			mSfx.setVolume(0.0f, 0.0f);
		}

	}

	@Override
	protected void onStop() {
		super.onStop();
		if (mSfx != null) {
			mSfx.stop();
			mSfx.release();
			mSfx = null;
		} 
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		isVolumeOn = true;
		mSfx = MediaPlayer.create(this, R.raw.sfx00);
		mSfx.start();
	}

	public void readAlong(View view) {
		if (mSfx.isPlaying()) {
			mSfx.stop();
			mSfx.release();
			mSfx = null;
			
			Intent i = new Intent(this, Page01Activity.class);
			i.putExtra("volume", KidAndTigerApp.getInstance().getVolumeFlag());
			i.putExtra("bg_volume", KidAndTigerApp.getInstance().getNarratorFlag());
			startActivity(i);
		}

	}

	public void showSettings(View view) {
		mSettings2.show(getFragmentManager(), "settings2_dialog");
	}
	
	public void showAbout(View view) {
		mAbout.show(getFragmentManager(), "about_dialog");
	}

	public void setVolume(View view) {
		
		if (isVolumeOn) {
			sound.setBackgroundResource(R.drawable.sound_mute);
			mSfx.setVolume(0.0f, 0.0f); // turn off
			isVolumeOn = false;
			KidAndTigerApp.getInstance().setVolumeFlag(false);
		} else {
			sound.setBackgroundResource(R.drawable.sounds_style);
			mSfx.setVolume(1.0f, 1.0f); // turn on
			isVolumeOn = true;
			KidAndTigerApp.getInstance().setVolumeFlag(true);
		}
	}

}
