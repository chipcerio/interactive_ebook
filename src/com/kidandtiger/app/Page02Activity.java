package com.kidandtiger.app;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.kidandtiger.R;
import com.kidandtiger.util.DebugLog;

public class Page02Activity extends Activity {
	protected MediaPlayer mSfx;
	protected MediaPlayer mNarrator;
	private PagesDialog mPagesDialog;
	private boolean isVolumeOn;
	private boolean isNarratorOn;
	private Handler mHandler;
	private TextView mText;
	private Button sound;
	private static final String TAG = "Page02Activity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.a_page02);
		isVolumeOn = getIntent().getBooleanExtra("volume", true);
		isNarratorOn = getIntent().getBooleanExtra("bg_volume", true);

		mText = (TextView) findViewById(R.id.narration_page02);
		mSfx = MediaPlayer.create(this, R.raw.sfx02);
		mNarrator = MediaPlayer.create(this, R.raw.page2);
		mPagesDialog = new PagesDialog(isVolumeOn, isNarratorOn);
		sound = (Button) findViewById(R.id.volume_page02);

		mSfx.setLooping(true);
		mSfx.start();
		mNarrator.start();

		if (isVolumeOn) {
			sound.setBackgroundResource(R.drawable.sounds_style);
			mSfx.setVolume(1.0f, 1.0f);
		} else {
			sound.setBackgroundResource(R.drawable.sound_mute);
			mSfx.setVolume(0.0f, 0.0f);
		}
		if (isNarratorOn)
			mNarrator.setVolume(1.0f, 1.0f);
		else
			mNarrator.setVolume(0.0f, 0.0f);

		mHandler = new Handler();
		mHandler.post(mRun);

		mPagesDialog.setAudio(mSfx, mNarrator);

		if (mNarrator.isPlaying()) {
			new Thread(new Runnable() {

				@Override
				public void run() {
					try {
						int duration = mNarrator.getDuration();
						Thread.sleep(duration
								+ KidAndTigerApp.getInstance().getInterval());

						if (mSfx != null) {
						    mSfx.stop();
						    mSfx.release();
						    mSfx = null;
                        }

						if (mNarrator != null) {
						    mNarrator.stop();
						    mNarrator.release();
						    mNarrator = null;
                        }

						Intent i = new Intent(Page02Activity.this,
								Page03Activity.class);
						i.putExtra("volume", KidAndTigerApp.getInstance()
								.getVolumeFlag());
						i.putExtra("bg_volume", KidAndTigerApp.getInstance()
								.getNarratorFlag());
						startActivity(i);
						finish();
						duration = 0;
						Thread.currentThread().interrupt();

					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}).start();
		}

	}

	private int mInterval = 0;
	private Runnable mRun = new Runnable() {

		@Override
		public void run() {
			if (mInterval == 7)
				mText.setText(R.string.page02_02);
			else if (mInterval == 12)
				mText.setText(R.string.page02_03);
			else if (mInterval == 16)
				mText.setText(R.string.page02_04);

			DebugLog.d(TAG, "count=" + mInterval++);
			mHandler.postDelayed(this, 1000);
		}
	};

	@Override
	protected void onStop() {
		super.onStop();
		if (mSfx != null) {
			mSfx.stop();
			mSfx.release();
			mSfx = null;

			mNarrator.stop();
			mNarrator.release();
			mNarrator = null;
		}
		mHandler.removeCallbacks(mRun);
		mInterval = 0;

		if (mPagesDialog.isAdded()) {
			mPagesDialog.dismiss();
		}

	}

	@Override
	protected void onRestart() {
		super.onRestart();
		isVolumeOn = true;
		KidAndTigerApp.getInstance().setVolumeFlag(true);
		mSfx = MediaPlayer.create(this, R.raw.sfx02);
		mNarrator = MediaPlayer.create(this, R.raw.page2);
		mSfx.start();
		mNarrator.start();

		mHandler.post(mRun);
		mInterval = 0;
		mText.setText(R.string.page02_01);
		mPagesDialog = new PagesDialog(isVolumeOn, isNarratorOn);
		mPagesDialog.setAudio(mSfx, mNarrator);
	}

	public void navigate(View view) {
		switch (view.getId()) {
		case R.id.volume_page02:
			if (isVolumeOn) {
				sound.setBackgroundResource(R.drawable.sound_mute);
				mSfx.setVolume(0.0f, 0.0f); // turn off
				mNarrator.setVolume(0.0f, 0.0f);
				isVolumeOn = false;
				KidAndTigerApp.getInstance().setVolumeFlag(false);
				KidAndTigerApp.getInstance().setNarratorFlag(false);
			} else {
				sound.setBackgroundResource(R.drawable.sounds_style);
				mSfx.setVolume(1.0f, 1.0f); // turn on
				mNarrator.setVolume(1.0f, 1.0f);
				isVolumeOn = true;
				KidAndTigerApp.getInstance().setVolumeFlag(true);
				KidAndTigerApp.getInstance().setNarratorFlag(true);
			}

			break;

		case R.id.tiger_page02:
			mPagesDialog.show(getFragmentManager(), "pages_dialog");
			break;

		case R.id.previous_page02:
			mSfx.stop();
			mSfx.release();
			mSfx = null;

			mNarrator.stop();
			mNarrator.release();
			mNarrator = null;

			Intent j = new Intent(this, Page01Activity.class);
			j.putExtra("volume", KidAndTigerApp.getInstance().getVolumeFlag());
			j.putExtra("bg_volume", KidAndTigerApp.getInstance()
					.getNarratorFlag());
			startActivity(j);
			break;

		case R.id.narration_page02:
			break;

		case R.id.next_page02:
		    if (mSfx != null) {
		        mSfx.stop();
		        mSfx.release();
		        mSfx = null;
		    }

			mNarrator.stop();
			mNarrator.release();
			mNarrator = null;

			Intent i = new Intent(this, Page03Activity.class);
			i.putExtra("volume", KidAndTigerApp.getInstance().getVolumeFlag());
			i.putExtra("bg_volume", KidAndTigerApp.getInstance()
					.getNarratorFlag());
			startActivity(i);
			finish();
			break;

		default:
			break;
		}
	}

}
