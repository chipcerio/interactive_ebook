package com.kidandtiger.app;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.kidandtiger.R;

public class Page04Activity extends Activity {
	protected MediaPlayer mSfx;
	protected MediaPlayer mNarrator;
	private PagesDialog mPagesDialog;
	private boolean isVolumeOn;
	private boolean isNarratorOn;
	private Button sound;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.a_page04);
		isVolumeOn = getIntent().getBooleanExtra("volume", true);
		isNarratorOn = getIntent().getBooleanExtra("bg_volume", true);
		sound = (Button)findViewById(R.id.volume_page04);
		mSfx = MediaPlayer.create(this, R.raw.sfx04);
		mNarrator = MediaPlayer.create(this, R.raw.page4);
		mPagesDialog = new PagesDialog(isVolumeOn, isNarratorOn);
		
		mSfx.setLooping(true); mSfx.start();
		mNarrator.start();
		
		if (isVolumeOn) {
			sound.setBackgroundResource(R.drawable.sounds_style);
			mSfx.setVolume(1.0f, 1.0f);
		} else {
			sound.setBackgroundResource(R.drawable.sound_mute);
			mSfx.setVolume(0.0f, 0.0f);
		}
		if (isNarratorOn)
			mNarrator.setVolume(1.0f, 1.0f);
		else
			mNarrator.setVolume(0.0f, 0.0f);
		
		mPagesDialog.setAudio(mSfx, mNarrator);

		if (mNarrator.isPlaying()) {
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					try {
						int duration = mNarrator.getDuration();
						Thread.sleep(duration + KidAndTigerApp.getInstance().getInterval());
						
						if (mSfx != null) {
                            mSfx.stop();
                            mSfx.release();
                            mSfx = null;
                        }

                        if (mNarrator != null) {
                            mNarrator.stop();
                            mNarrator.release();
                            mNarrator = null;
                        }
						
						Intent i = new Intent(Page04Activity.this, Page05Activity.class);
						i.putExtra("volume", KidAndTigerApp.getInstance().getVolumeFlag());
						i.putExtra("bg_volume", KidAndTigerApp.getInstance().getNarratorFlag());
						startActivity(i);
						finish();
						duration = 0;
						Thread.currentThread().interrupt();
						
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}).start();
		}
		
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		if (mSfx != null) {
			mSfx.stop();
			mSfx.release();
			mSfx = null;
			
			mNarrator.stop();
			mNarrator.release(); 
			mNarrator = null;
		} 
		
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		isVolumeOn = true;
		mSfx = MediaPlayer.create(this, R.raw.sfx04);
		mNarrator = MediaPlayer.create(this, R.raw.page4);
		mSfx.start();
		mNarrator.start();
		
	}
	
	public void navigate(View view) {
		switch (view.getId()) {
		case R.id.volume_page04:
			if (isVolumeOn) {
				sound.setBackgroundResource(R.drawable.sound_mute);
				mSfx.setVolume(0.0f, 0.0f); // turn off
				mNarrator.setVolume(0.0f, 0.0f);
				isVolumeOn = false;
				KidAndTigerApp.getInstance().setVolumeFlag(false);
				KidAndTigerApp.getInstance().setNarratorFlag(false);
			} else {
				sound.setBackgroundResource(R.drawable.sounds_style);
				mSfx.setVolume(1.0f, 1.0f); // turn on
				mNarrator.setVolume(1.0f, 1.0f);
				isVolumeOn = true;
				KidAndTigerApp.getInstance().setVolumeFlag(true);
				KidAndTigerApp.getInstance().setNarratorFlag(true);
			}
			break;
			
		case R.id.tiger_page04:
			mPagesDialog.show(getFragmentManager(), "pages_dialog");
			break;
			
		case R.id.previous_page04:
			mSfx.stop(); mSfx.release();
			mNarrator.stop(); mNarrator.release();
			
			mSfx = null;
			mNarrator = null;
			
			Intent j = new Intent(this, Page03Activity.class);
			j.putExtra("volume", KidAndTigerApp.getInstance().getVolumeFlag());
			j.putExtra("bg_volume", KidAndTigerApp.getInstance().getNarratorFlag());
			startActivity(j);
			break;
			
		case R.id.narration_page04:
			break;
			
		case R.id.next_page04:
			mSfx.stop(); mSfx.release();
			mNarrator.stop(); mNarrator.release();
			
			mSfx = null;
			mNarrator = null;
			
			Intent i = new Intent(this, Page05Activity.class);
			i.putExtra("volume", KidAndTigerApp.getInstance().getVolumeFlag());
			i.putExtra("bg_volume", KidAndTigerApp.getInstance().getNarratorFlag());
			startActivity(i);
			finish();
			break;

		default:
			break;
		}
	}

}
