package com.kidandtiger.app;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;

import com.kidandtiger.R;
import com.kidandtiger.util.DebugLog;

public class PagesDialog extends DialogFragment 
implements View.OnClickListener, OnCheckedChangeListener, OnItemSelectedListener {
	private boolean isVolumeOn;
	private boolean isNarratorOn;
	private ImageButton mPage01;
	private ImageButton mPage02;
	private ImageButton mPage03;
	private ImageButton mPage04;
	private ImageButton mPage05;
	private ImageButton mPage06;
	private ImageButton mPage07;
	private ImageButton mPage08;
	private LinearLayout pages;
	private FrameLayout mLayoutSettings;
	private Button mHome;
	private Button settings;
	private Button mPages;
	private SettingsDialog mSettings;
	private Switch mNarratorSwitch;
	private Switch mBackgroundSwitch;
	private Spinner mSpinner;
	
	private MediaPlayer mBackgroundAudio;
	private MediaPlayer mNarratorAudio;
	
	public PagesDialog(boolean hasBackground, boolean hasNarrator) {
		isVolumeOn = hasBackground;
		isNarratorOn = hasNarrator;
		KidAndTigerApp.getInstance().setVolumeFlag(hasBackground);
		KidAndTigerApp.getInstance().setNarratorFlag(hasNarrator);
	}
	
	public void setAudio(MediaPlayer bgMusic, MediaPlayer narratorAudio) {
		mBackgroundAudio = bgMusic;
		mNarratorAudio = narratorAudio;
	}
	
	@Override
	public View onCreateView(LayoutInflater i, ViewGroup c, Bundle s) {
		View view = i.inflate(R.layout.f_pages, c);
		getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
		mSettings = new SettingsDialog();
		getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
		mPage01 = (ImageButton) view.findViewById(R.id.img_page01);
		mPage02 = (ImageButton) view.findViewById(R.id.img_page02);
		mPage03 = (ImageButton) view.findViewById(R.id.img_page03);
		mPage04 = (ImageButton) view.findViewById(R.id.img_page04);
		mPage05 = (ImageButton) view.findViewById(R.id.img_page05);
		mPage06 = (ImageButton) view.findViewById(R.id.img_page06);
		mPage07 = (ImageButton) view.findViewById(R.id.img_page07);
		mPage08 = (ImageButton) view.findViewById(R.id.img_page08);
		pages = (LinearLayout) view.findViewById(R.id.LayoutPages);
		mLayoutSettings = (FrameLayout) view.findViewById(R.id.layout_settings);
		mNarratorSwitch = (Switch) view.findViewById(R.id.hidden_settings_narrator_switch);
		mBackgroundSwitch = (Switch) view.findViewById(R.id.hidden_settings_background_switch);
		mSpinner = (Spinner) view.findViewById(R.id.spinner);
	

		mPage01.setOnClickListener(this);
		mPage02.setOnClickListener(this);
		mPage03.setOnClickListener(this);
		mPage04.setOnClickListener(this);
		mPage05.setOnClickListener(this);
		mPage06.setOnClickListener(this);
		mPage07.setOnClickListener(this);
		mPage08.setOnClickListener(this);

		mHome = (Button) view.findViewById(R.id.hidden_btn_home);
		mPages = (Button) view.findViewById(R.id.hidden_btn_pages);
		settings = (Button)view.findViewById(R.id.hidden_btn_settings);
		settings.setOnClickListener(this);
		mHome.setOnClickListener(this);
		mPages.setOnClickListener(this);
		
		
		mNarratorSwitch.setOnCheckedChangeListener(this);
		mBackgroundSwitch.setOnCheckedChangeListener(this);
		mSpinner.setOnItemSelectedListener(this);
		mNarratorSwitch.setChecked(KidAndTigerApp.getInstance().getNarratorFlag());
		mBackgroundSwitch.setChecked(KidAndTigerApp.getInstance().getVolumeFlag());
		
		return view;
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.img_page01:
			jumpTo(Page01Activity.class);
			break;

		case R.id.img_page02:
			jumpTo(Page02Activity.class);
			break;

		case R.id.img_page03:
			jumpTo(Page03Activity.class);
			break;

		case R.id.img_page04:
			jumpTo(Page04Activity.class);
			break;

		case R.id.img_page05:
			jumpTo(Page05Activity.class);
			break;

		case R.id.img_page06:
			jumpTo(Page06Activity.class);
			break;

		case R.id.img_page07:
			jumpTo(Page07Activity.class);
			break;

		case R.id.img_page08:
			jumpTo(Page08Activity.class);
			break;

		case R.id.hidden_btn_home:
			jumpTo(MainActivity.class);
			break;

		case R.id.hidden_btn_pages:
			if (pages.getVisibility() == View.INVISIBLE) {
				pages.setVisibility(View.VISIBLE);
			} 
			else if(mLayoutSettings.getVisibility() == View.VISIBLE){
				pages.setVisibility(View.VISIBLE);
				mLayoutSettings.setVisibility(View.GONE);
			}
			
			else {
				pages.setVisibility(View.INVISIBLE);
			}
			break;
		
		case R.id.hidden_btn_settings:
			if (mLayoutSettings.getVisibility() == View.GONE && pages.getVisibility() == View.INVISIBLE) {
				mLayoutSettings.setVisibility(View.VISIBLE);
				pages.setVisibility(View.GONE);
			}
			else if(pages.getVisibility()==View.VISIBLE){
				pages.setVisibility(View.GONE);
				mLayoutSettings.setVisibility(View.VISIBLE);
			}
			
			else {
				pages.setVisibility(View.INVISIBLE);
				mLayoutSettings.setVisibility(View.GONE);
			}
			break;
			
		default:
			break;
		}
	}

	public void showSettings() { // TODO i think this is not used
		this.dismiss();
		mSettings.show(getFragmentManager(), "settings_dialog");
	}
	
	public void set() {          // TODO i think this is not used
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.r_settings_dialog);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
		dialog.show();
	}

	private void jumpTo(Class<?> cls) {
		Intent i = new Intent(getActivity(), cls);
		i.putExtra("volume", isVolumeOn);// TODO i think this is not used
		i.putExtra("bg_volume", isNarratorOn);
		getActivity().startActivity(i);
		getActivity().finish();
	}

	private static final String TAG = "PagesDialog";

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (buttonView.getId() == R.id.hidden_settings_narrator_switch) {
			if(isChecked) {
				mNarratorSwitch.setChecked(true);
				if (mNarratorAudio != null) {
					mNarratorAudio.setVolume(1.0f, 1.0f);
					KidAndTigerApp.getInstance().setNarratorFlag(true);
				}
				;
				
			} else {
				mNarratorSwitch.setChecked(false);
				if (mNarratorAudio != null) {
					mNarratorAudio.setVolume(0.0f, 0.0f);
					KidAndTigerApp.getInstance().setNarratorFlag(false);
				}
				;
				
			}
			
		
		} else if (buttonView.getId() == R.id.hidden_settings_background_switch) {
			if (isChecked) {
				mBackgroundSwitch.setChecked(true);
				if (mBackgroundAudio != null) {
					mBackgroundAudio.setVolume(1.0f, 1.0f);
					KidAndTigerApp.getInstance().setVolumeFlag(true);
					DebugLog.d(TAG, "VOLUME=ON");
				}
				
			} else {
				mBackgroundSwitch.setChecked(false);
				if (mBackgroundAudio != null) {
					mBackgroundAudio.setVolume(0.0f, 0.0f);
					KidAndTigerApp.getInstance().setVolumeFlag(false);
					DebugLog.d(TAG, "VOLUME=OFF");
				}
				
			}
			
		}
		
		
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		if (parent.getItemAtPosition(position).toString().contentEquals("5 Seconds")) {
			KidAndTigerApp.getInstance().setInterval(5000);
		} else if (parent.getItemAtPosition(position).toString().contentEquals("10 Seconds")) {
			KidAndTigerApp.getInstance().setInterval(10000);
		} else if (parent.getItemAtPosition(position).toString().contentEquals("15 Seconds")) {
			KidAndTigerApp.getInstance().setInterval(15000);
		} else if (parent.getItemAtPosition(position).toString().contentEquals("20 Seconds")) {
			KidAndTigerApp.getInstance().setInterval(20000);
		}
		
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// do nothing
	}

}