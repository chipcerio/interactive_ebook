package com.kidandtiger.app;

import android.app.DialogFragment;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.kidandtiger.R;

public class SettingsDialog extends DialogFragment implements
		RadioGroup.OnCheckedChangeListener {

	private RadioGroup mRGNarration;
	private RadioGroup mRGMusic;
	private RadioGroup mRGAutofill;
	private RadioGroup mRGSeconds;

	private RadioButton mNarrationOn;
	private RadioButton mNarrationOff;

	private RadioButton mMusicOn;
	private RadioButton mMusicOff;
	private RadioButton m5SecRadio;
	private RadioButton m10SecRadio;
	private RadioButton m15SecRadio;
	private RadioButton m20SecRadio;
	private MainActivity mMain;
	private boolean isVolumeOn;
	private boolean isNarratorOn = true;
	private PagesDialog pagesDialog;
	
	public SettingsDialog(MainActivity main, boolean volume) {
		mMain = main;
		isVolumeOn = volume;
	}
	
	public SettingsDialog(boolean volume) {
		isVolumeOn = volume;
	}
	
	public SettingsDialog() {
		isVolumeOn = KidAndTigerApp.getInstance().getVolumeFlag();
		isNarratorOn = KidAndTigerApp.getInstance().getNarratorFlag();
	}
	
	@Override
	public View onCreateView(LayoutInflater i, ViewGroup c, Bundle s) {
		View view = i.inflate(R.layout.r_settings_dialog, c);
		getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
		getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
		init(view);
		
		if (isVolumeOn) {
			mNarrationOn.setChecked(true) ; // TODO
		} else 
			mNarrationOff.setChecked(true);
		
		return view;
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {

		if (group.getId() == R.id.rgNarration) {
			switch (checkedId) {
			case R.id.btn_narration_on:
				mNarrationOn.setChecked(true);
				mMain.isNarratorOn = true;
				Log.e("Narration", "Narration On");
				break;

			case R.id.btn_narration_off:
				Log.e("Narration", "Narration Off");
				mNarrationOff.setChecked(true);
				mMain.isNarratorOn = false;
				break;
			default:
				break;
			}

		} else if (group.getId() == R.id.rgMusic) {
			switch (checkedId) {
			case R.id.btn_bg_music_on:
				Log.e("Music", "Music On");
				mMusicOn.setChecked(true);
				mMain.mSfx.setVolume(1.0f, 1.0f);
				break;
			case R.id.btn_bg_music_off:
				Log.e("Music", "Music Off");
				mMusicOff.setChecked(true);
				mMain.isVolumeOn = false;
				mMain.mSfx.setVolume(0.0f, 0.0f);
				break;
			default:
				break;
			}

		} else if (group.getId() == R.id.rgAutofill) {
			switch (checkedId) {
			case R.id.btn_fill_on:

				break;

			case R.id.btn_fill_off:
				break;
			default:
				break;
			}

		} else if (group.getId() == R.id.rgSeconds) {
			switch (checkedId) {
			case R.id.radio_5sec:
				break;

			case R.id.radio_10sec:
				break;
			case R.id.radio_15sec:
				break;
			case R.id.radio_20sec:
				break;

			default:
				break;
			}

		}

	}

	private void init(View view) {
		mRGNarration = (RadioGroup) view.findViewById(R.id.rgNarration);
		mRGMusic = (RadioGroup) view.findViewById(R.id.rgMusic);
		mRGAutofill = (RadioGroup) view.findViewById(R.id.rgAutofill);
		mRGSeconds = (RadioGroup) view.findViewById(R.id.rgSeconds);

		mNarrationOn = (RadioButton) view.findViewById(R.id.btn_narration_on);
		mNarrationOff = (RadioButton) view.findViewById(R.id.btn_narration_off);

		mMusicOn = (RadioButton) view.findViewById(R.id.btn_bg_music_on);
		mMusicOff = (RadioButton) view.findViewById(R.id.btn_bg_music_off);

		m5SecRadio = (RadioButton) view.findViewById(R.id.radio_5sec);
		m10SecRadio = (RadioButton) view.findViewById(R.id.radio_10sec);
		m15SecRadio = (RadioButton) view.findViewById(R.id.radio_15sec);
		m20SecRadio = (RadioButton) view.findViewById(R.id.radio_20sec);

		
		mRGNarration.setOnCheckedChangeListener(this);
		mRGMusic.setOnCheckedChangeListener(this);
		mRGAutofill.setOnCheckedChangeListener(this);
		mRGSeconds.setOnCheckedChangeListener(this);
		
		mNarrationOn.setChecked(mMain.isNarratorOn);
		mMusicOn.setChecked(mMain.isVolumeOn);

		m5SecRadio.setEnabled(false);
		m10SecRadio.setEnabled(false);
		m15SecRadio.setEnabled(false);
		m20SecRadio.setEnabled(false);
	}

}