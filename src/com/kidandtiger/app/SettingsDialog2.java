package com.kidandtiger.app;

import android.app.DialogFragment;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.Switch;

import com.kidandtiger.R;
import com.kidandtiger.util.DebugLog;

public class SettingsDialog2 extends DialogFragment 
implements OnCheckedChangeListener, OnItemSelectedListener {
	private Switch mNarratorSwitch;
	private Switch mBackgroundSwitch;
	private Spinner mIntervalSpinner;
	private boolean isVolumeOn;
	private boolean isNarratorOn;
	private MainActivity mMain;
	
	private static final String TAG = "SettingsDialog2";
	
	public SettingsDialog2(MainActivity main) {
		mMain = main;
	}

	@Override
	public View onCreateView(LayoutInflater i, ViewGroup c, Bundle s) {
		View view = i.inflate(R.layout.f_settings2, c);
		getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
		getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
		
		isVolumeOn = KidAndTigerApp.getInstance().getVolumeFlag();
		isNarratorOn = KidAndTigerApp.getInstance().getNarratorFlag();
		init(view);
		
		return view;
	}
	
	private void init(View view) {
		/*
		 * Switch on/off findviewbyid
		 * Switch on/off findviewbyid
		 * Spinner findviewbyid
		 */
		mNarratorSwitch = (Switch) view.findViewById(R.id.main_settings_narration_switch);
		mBackgroundSwitch = (Switch) view.findViewById(R.id.main_settings_background_switch);
		mIntervalSpinner = (Spinner) view.findViewById(R.id.spinner);
		
		/*
		 * set listeners
		 */
		mNarratorSwitch.setOnCheckedChangeListener(this);
		mBackgroundSwitch.setOnCheckedChangeListener(this);
		mIntervalSpinner.setOnItemSelectedListener(this);
		
		/*
		 * set checked/unchecked
		 */
		mNarratorSwitch.setChecked(isNarratorOn);
		mBackgroundSwitch.setChecked(isVolumeOn);
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		/*
		 * what happens if checked changed on switches?
		 */
		if (buttonView.getId() == R.id.main_settings_narration_switch) {
			DebugLog.d(TAG, "pressed narration switch");
			DebugLog.d(TAG, "isChecked="+ isChecked);
			// TODO narrator on/off is not used. no narrator on Home page
			
		} else if (buttonView.getId() == R.id.main_settings_background_switch) {
			DebugLog.d(TAG, "pressed background switch");
			if (isChecked) {
				mBackgroundSwitch.setChecked(true);
				mMain.mSfx.setVolume(1.0f, 1.0f);
				KidAndTigerApp.getInstance().setVolumeFlag(true);
			} else {
				mBackgroundSwitch.setChecked(false);
				mMain.mSfx.setVolume(0.0f, 0.0f);
				KidAndTigerApp.getInstance().setVolumeFlag(false);
			}
			
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
		/*
		 * what happends if drop down item is selected?
		 * set global value for time millis
		 */
		if (parent.getItemAtPosition(pos).toString().contentEquals("5 Seconds")) {
			KidAndTigerApp.getInstance().setInterval(5000);
			DebugLog.d(TAG, "interval="+KidAndTigerApp.getInstance().getInterval());
		} else if (parent.getItemAtPosition(pos).toString().contentEquals("10 Seconds")) {
			KidAndTigerApp.getInstance().setInterval(10000);
			DebugLog.d(TAG, "interval="+KidAndTigerApp.getInstance().getInterval());
		} else if (parent.getItemAtPosition(pos).toString().contentEquals("15 Seconds")) {
			KidAndTigerApp.getInstance().setInterval(15000);
			DebugLog.d(TAG, "interval="+KidAndTigerApp.getInstance().getInterval());
		} else if (parent.getItemAtPosition(pos).toString().contentEquals("20 Seconds")) {
			KidAndTigerApp.getInstance().setInterval(20000);
			DebugLog.d(TAG, "interval="+KidAndTigerApp.getInstance().getInterval());
		} else if (parent.getItemAtPosition(pos).toString().contentEquals("OFF")) {
			KidAndTigerApp.getInstance().setInterval(3600000); // 1 hr = 3600000 millis
			DebugLog.d(TAG, "interval="+KidAndTigerApp.getInstance().getInterval());
		}
		
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) { // do nothing
	}

}